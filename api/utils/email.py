from email_validator import EmailNotValidError, EmailSyntaxError, validate_email


def check_email(email):
    try:
        validate_email(email, check_deliverability=True)
        return True
    except EmailSyntaxError or EmailNotValidError:
        return False
