from fastapi import Depends, HTTPException
from ..utils.oauth2 import oauth2_scheme
from ..utils.token import verify_token


async def authenticate(token: str = Depends(oauth2_scheme)):
    data = verify_token(token)
    if not data:
        raise HTTPException(status_code=401, detail="Authentication failed")
    return data
