import jwt
from jwt.exceptions import PyJWTError
import os

# Set these two variables in production! The defaults are for development only!
SECRET_KEY = os.environ.get('API_SECRET', '3ffb83904f32d22041df4f6a19984106c9627ea646a5b5aa5b22296c55e8fd54')
ALGORITHM = os.environ.get('API_ALGORITHM', 'HS256')


def create_token(data):
    return jwt.encode(data, key=SECRET_KEY, algorithm=ALGORITHM)


def verify_token(token):
    try:
        return jwt.decode(token, key=SECRET_KEY, algorithms=ALGORITHM)
    except PyJWTError:
        return False
