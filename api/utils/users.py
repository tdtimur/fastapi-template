from api.database import async_mongo as db


async def check_user(username):
    return False if not await db.users.find_one({'username': username}) else True


async def verify_user(username, password_hash):
    return False if not await db.users.find_one(
        {'$and': [{'username': username}, {'hash': password_hash}]}, {'_id': 0}
    ) else True
