from fastapi import APIRouter, Depends
from ..database import async_mongo as db
from ..utils.dependencies import authenticate


router = APIRouter()


@router.get("/")
async def get_users(user: dict = Depends(authenticate)):
    return {'status': 'success', 'users': await db.users.find({}, {'_id': 0, 'password': 0}).to_list(length=None)}
