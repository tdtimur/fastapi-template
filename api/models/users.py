from pydantic import BaseModel


class Register(BaseModel):
    full_name: str
    username: str
    email: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class Users(BaseModel):
    full_name: str
    username: str
    email: str
    hash: str
    avatar: str
