from __future__ import absolute_import
from fastapi import FastAPI
import logging
from starlette.middleware.cors import CORSMiddleware
from api.database import mongo as db
from api.models.users import Users
from api.routes import root, users
from api.utils.hash import password_hash

app = FastAPI(title='FastAPI Template with MongoDB', version='1.0.1')
logger = logging.getLogger('uvicorn')

app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=["*"], allow_headers=["*"])
app.include_router(root.router, prefix='', tags=['root'])
app.include_router(users.router, prefix='/users', tags=['users'])


@app.on_event('startup')
async def prepare():  # pragma: no cover
    admin = Users(
        full_name='Administrator',
        username='admin',
        email='admin@api.com',
        hash=password_hash('admin'),
        avatar=''
    )
    if db.users.find_one({'username': 'admin'}) is None:
        db.users.insert_one(admin.dict())
    else:
        logger.info('User admin already exists')
