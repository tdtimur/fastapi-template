import os
from pymongo import MongoClient

host = os.environ.get('API_DB_HOST', 'localhost')

client = MongoClient(host=host)
database = client.api
users = database.users
