import os
from motor.motor_asyncio import AsyncIOMotorClient

host = os.environ.get('API_DB_HOST', 'localhost')

client = AsyncIOMotorClient(host)
database = client.api
users = database.users
